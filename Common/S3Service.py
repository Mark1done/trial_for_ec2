__author__ = "MArK"

import boto3
from STSService import STSService
from config import config


class S3Service(object):
    """
    Class for s3 resource and interactions
    """
    def __init__(self):
        """
        Constructor.

        """
        self.__resource = self.create_resource()

    def create_resource(self):
        """
        Creates a s3 resource instance.

        :return: s3 resource
        """
        # assume role of ec2 for access to s3
        sts_client = STSService()
        s3_assume_role_object = sts_client.get_client().assume_role(
            RoleArn=config['s3_role_arn'],
            RoleSessionName="s3_assume_role_session"
        )
        self.__credentials = s3_assume_role_object['credentials']
        s3_resource = boto3.resource(
            's3',
            aws_access_key_id=self.__credentials['AccessKeyId'],
            aws_secret_access_key=self.__credentials['SecretAccessKey'],
            aws_session_token=self.__credentials['SessionToken'],
        )
        return s3_resource

    def get_resource(self):
        """
        Returns the resource instance.

        :return: s3 resource
        """
        return self.__resource
