__author__ = "MArK"

import boto3


class STSService(object):
    """
    Class for sts client and interactions.

    """
    def __init__(self):
        """
        Constructor.

        :param arn: the role
        :param role_session_name: the name given to the assume role session
        """
        self.__client = self.create()

    def create(self):
        """
        Creates the sts client.

        :return: sts client
        """
        sts_client = boto3.client('sts')
        return sts_client

    def get_client(self):
        """
        Returns the sts client.

        :return: sts client
        """
        return self.__client
