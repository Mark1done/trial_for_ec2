__author__ = "MArK"

import boto3
from STSService import STSService
from config import config


class RDSService(object):
    """
    Class for rds resource and interactions.

    """
    def __init__(self):
        """
        Constructor.
        """
        self.__resource = self.create_resource()

    def create_resource(self):
        """
        Creates a new MySql database.

        :return: rds resoruce
        """
        # assume role of ec2 for access to rds
        sts_client = STSService()
        rds_assume_role_object = sts_client.get_client().assume_role(
            RoleArn=config["rds_role_arn"],
            RoleSessionName="rds_assume_role_session"
        )
        self.__credentials = rds_assume_role_object['credentials']
        rds_resource = boto3.resource(
            'rds',
            aws_access_key_id=self.__credentials['AccessKeyId'],
            aws_secret_access_key=self.__credentials['SecretAccessKey'],
            aws_session_token=self.__credentials['SessionToken']
        )
        return rds_resource

    def get_resource(self):
        """
        Returns the resource instance.

        :return: rds resource
        """
        return self.__resource
