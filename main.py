__author__ = "MArK"

from flask import Flask
from flask_restful import Api
from Resources.Hello import Hello
from Resources.Upload import Upload

app = Flask(__name__)
api = Api(app)

# hello world
api.add_resource(Hello, '/')
api.add_resource(Upload, '/upload')


if __name__ == "__main__":
    app.run(port=8000, debug=True)



# todo resources, common
