__author__ = "MArK"

from flask_restful import Resource
from flask import request
from Common.S3Service import S3Service
from Common.RDSService import RDSService

class Upload(Resource):
    def post(self):
        """
        Uploads the file into an ec2 bucket.

        :return:
        """
        # check payload for data
        try:
            data = request.files['file']

        except:
            return {"message": "There was no file"}, 510

        # save to s3
        s3_resource = S3Service().get_resource()
        buckets = s3_resource.buckets.all()
        bucket = buckets[0]
        bucket.upload_file(data.filename, data.filename)

        # keep record in rds
        rds_resource = RDSService().get_resource()

        return {"message": "Successful upload"}, 204
